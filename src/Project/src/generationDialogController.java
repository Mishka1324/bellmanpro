package controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * Created by User on 29.06.2016.
 */
public class generationDialogController {

    @FXML
    TextField generationText;

    private boolean isDigit(String text)
    {
        try{
            int number = Integer.parseInt(text);
            return true;
        }
        catch (NumberFormatException e) {
            return false;
        }
    }

    public void gdOK(ActionEvent actionEvent) {
        System.out.println("Ok was pressed");
        System.out.println("Text " + generationText.getText() + " was entered.");
        String text = generationText.getText();
        int n;
        if (isDigit(text)) {
            n = Integer.parseInt(text);
            if (n > 0 && n < 11) {
                mainController.shouldGenerate = false;
                ((Stage) ((Node) actionEvent.getSource()).getScene().getWindow()).close();
            }
            else
            {
                generationText.clear();
                generationText.setPromptText(" 1 < n < 11 ");
            }
        }
        else
        {
            generationText.clear();
            generationText.setPromptText(" 1 < n < 11 ");
        }
    }

    public void gdCancel(ActionEvent actionEvent) {
        System.out.println("Cancle was pressed");
        mainController.shouldGenerate = false;
                ((Stage) ((Node) actionEvent.getSource()).getScene().getWindow()).close();
    }
}
