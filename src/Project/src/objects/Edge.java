package objects;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.shape.Line;

import java.io.File;
/**
 * Created by User on 29.06.2016.
 */
public class Edge {
    public Vertex v1;
    public Vertex v2;
    private double weight;
    public Line line;
    public boolean isChecked;

    public Edge(Vertex v1, Vertex v2, double weight) {
        this.v1 = v1;
        this.v2 = v2;
        this.weight = weight;
        this.isChecked = false;
        this.line = createLine (v1,v2);
    }

    public Line createLine(Vertex v1, Vertex v2) {
        Line line = new Line();
        //Polygon.
        line.setStartX(v1.circle.getX() + 15);
        line.setStartY(v1.circle.getY() + 15);
        line.setEndX(v2.circle.getX() + 15);
        line.setEndY(v2.circle.getY() + 15);
        line.setStyle("-fx-stroke-width:2");

        //Media sound = new Media(new File("").toURI().toString());
        //MediaPlayer mediaPlayer = new MediaPlayer(sound);
        //mediaPlayer.play();

        return line;
    }

    public Vertex getV1() {
        return this.v1;
    }

    public Vertex getV2() {
        return this.v2;
    }

    public double getWeight() {
        return this.weight;
    }
}
