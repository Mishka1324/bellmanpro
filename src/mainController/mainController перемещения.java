package controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;

import static java.lang.Math.sqrt;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import javafx.event.ActionEvent;

import javafx.stage.Modality;
import javafx.stage.Stage;
import objects.Vertex;
import objects.Edge;

//import fxml.*;

public class mainController {

    public static ArrayList<Vertex> buttons = new ArrayList<Vertex>();

    public static ArrayList<Edge> edges = new ArrayList<Edge>();

    public static ArrayList<Double> vec = new ArrayList<Double>(Vertex.i);

    public static boolean shouldBuild = false;
    public static boolean shouldGenerate = false;
    public static boolean shouldSetMatrix = false;
    public static boolean fordInWork = false;
    public static boolean nextStep = false;

    public static int shpEdgesCounter = 0;

    public static boolean last = false;
    public static int counter = 0;
    public static Vertex startVert;


    @FXML
    Pane activePane;

    @FXML
    public Button Step;

    public void createButton (MouseEvent event) {
        if (event.getClickCount() == 2)
            twoMouseClick(event);
        else
            oneMouseClick(event);

    }

    private void twoMouseClick(MouseEvent event)
    {
        if (createOnCorrectPlace(event)) { //���� ������ � ���������� �����
            ImageView image = new ImageView("content/drt1.png");
            Vertex vertex = new Vertex(image);
            vertex.circle.setX(event.getX()-10);
            vertex.circle.setY(event.getY() - 10);
            buttons.add(vertex);
            activePane.getChildren().add(vertex.circle);//������� �������
            vertex.circle.toFront();
        }

    }

    private boolean createOnCorrectPlace(MouseEvent event)
    {
        for (Vertex vertex: buttons) {
            double fromCenterToEvent = sqrt((event.getX() - vertex.circle.getX()-10) * (event.getX() - vertex.circle.getX()-10)
                    + (event.getY() - vertex.circle.getY()-10) * (event.getY() - vertex.circle.getY()-10));
            if (fromCenterToEvent <= 30) {
                return false;
            }
        }
        return  true;
    }

    public void oneMouseClick(MouseEvent event) {
        int countOfCheckedVertex = 0;
        Vertex[] vertexes = new Vertex[2];
        boolean shouldContinue = true;
        checkButton(event);
        for (Vertex vertex : buttons)
        {
            if (vertex.isChecked)
            {
                vertexes[countOfCheckedVertex] = vertex;
                countOfCheckedVertex++;
            }
            if (countOfCheckedVertex == 2)
            {
                shouldContinue = checkEdge(event, vertexes);
                countOfCheckedVertex = setNormalView();
                if(shouldContinue) {
                    createEdge(vertexes);
                    break;
                }
            }
        }
    }

    private void createEdge(Vertex[] vertexes) {
        double weight = sqrt(((vertexes[0].circle.getX() - vertexes[1].circle.getX())*(vertexes[0].circle.getX() - vertexes[1].circle.getX())) + ((vertexes[0].circle.getY() - vertexes[1].circle.getY())*(vertexes[0].circle.getY() - vertexes[1].circle.getY())));
        Edge edge = new Edge(vertexes[0], vertexes[1], weight);
        edges.add(edge);
       // Media sound = new Media(new File("src/content/Edge.wav").toURI().toString());
        //MediaPlayer mediaPlayer = new MediaPlayer(sound);
       // mediaPlayer.play();
        activePane.getChildren().add(edge.line);
        edge.line.toBack();
        System.out.println(edge.getWeight());
    }

    private int setNormalView()
    {
        for (Vertex vertex : buttons)
        {
            vertex.circle.setStyle("-fx-image:url('content/drt1.png');");
            vertex.isChecked = false;
        }
        return 0;
    }

    private void checkButton (MouseEvent event) {
        for (Vertex vertex : buttons)
        {
            if (onCorrectPlace(event,vertex))
            {
                if (vertex.isChecked)
                {
                    vertex.circle.setStyle("-fx-image:url('content/drt1.png');");
                    vertex.isChecked = false;
                }
                else
                {
                    vertex.circle.setStyle("-fx-image:url('content/drt2.png');");
                    vertex.isChecked = true;
                }
            }

        }
    }

    private boolean onCorrectPlace(MouseEvent event,Vertex vertex)
    {
        boolean inX = event.getX() < vertex.circle.getX() + 25 && event.getX() > vertex.circle.getX() - 25;
        boolean inY = event.getY() < vertex.circle.getY() + 25 && event.getY() > vertex.circle.getY() - 25;
        return inX&&inY;
    }

    private boolean checkEdge(MouseEvent event, Vertex[] vertexes) {
        boolean shouldContinue = true;
        for (int i = 0; i < edges.size(); i++)
        {
            if (twoVertexChecked(i,vertexes))
            {
                if ((!edges.get(i).isChecked) && shpEdgesCounter == 0)
                {
                    edges.get(i).line.setStyle("-fx-stroke-width:4;-fx-stroke:red");
                    //edges.get(i).line.setStyle("-fx-stroke:red");
                    System.out.println("Weight of this edge = " + edges.get(i).getWeight());
                    shpEdgesCounter++;
                    System.out.println(shpEdgesCounter);
                    edges.get(i).isChecked = true;
                }
                else
                {
                    if (edges.get(i).line.getStyle().equals("-fx-stroke-width:4;-fx-stroke:red")) {
                        shpEdgesCounter--;
                        edges.get(i).line.setStyle("-fx-stroke-width:2;-fx-stroke:orange");
                    }
                    else
                        edges.get(i).line.setStyle("-fx-stroke-width:2");
                    edges.get(i).isChecked = false;
                }
                shouldContinue = false;
            }
        }
        return shouldContinue;
    }

    private boolean twoVertexChecked(int i,Vertex[] vertexes)
    {
        return (edges.get(i).getV1().equals(vertexes[0])&&edges.get(i).getV2().equals(vertexes[1]))
                ||(edges.get(i).getV2().equals(vertexes[0])&&edges.get(i).getV1().equals(vertexes[1]));
    }

    public void buildGraph(ActionEvent actionEvent) {
        if (!(shouldGenerate || shouldSetMatrix))
        shouldBuild = true;
    }

    public void tryToBuild(MouseEvent event)  throws IOException{
        if (shouldBuild) {
            createButton(event);
            System.out.println("shouldBuild = true!");
        }
        else
        if (fordInWork)
        {
            checkStartVert(event);
        }
        else
            System.out.println("shouldBuild = false!");
    }

    public void endBuilding(ActionEvent actionEvent) {
        shouldBuild = false;
    }


    public void generateGraph(ActionEvent actionEvent) throws Exception {
        if (!(shouldBuild || shouldSetMatrix || fordInWork))
        shouldGenerate = true;
        if (shouldGenerate) {
            generation(actionEvent);
            System.out.println("shouldGenerate = true!");
        }
        else
            System.out.println("shouldGenerate = false!");
    }

    public void generation(ActionEvent actionEvent) throws Exception{
            Stage stage = new Stage();
            Parent panel = FXMLLoader.load(getClass().getResource("../fxml/generationDialog.fxml"));
            stage.setTitle("��������� ���������");
            stage.setMinWidth(200);
            stage.setMinHeight(150);
            stage.setResizable(false);
            stage.setScene(new Scene(panel));
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(((Node)actionEvent.getSource()).getScene().getWindow());
            stage.show();
    }


    public void enterMatrix(ActionEvent actionEvent) throws Exception {
        if (!(shouldBuild || shouldGenerate || fordInWork || nextStep))
            shouldSetMatrix = true;
        if (shouldSetMatrix) {
            settingMatrixElements(actionEvent);
            System.out.println("shouldGenerate = true!");
        }
        else
            System.out.println("shouldGenerate = false!");
    }

    public void settingMatrixElements(ActionEvent actionEvent) throws Exception{
        Stage stage = new Stage();
        Parent panel = FXMLLoader.load(getClass().getResource("../fxml/matrixDialog.fxml"));
        stage.setTitle("���� ��������� �������");
        stage.setMinWidth(200);
        stage.setMinHeight(150);
        stage.setResizable(false);
        stage.setScene(new Scene(panel));
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(((Node)actionEvent.getSource()).getScene().getWindow());
        stage.show();
    }

    public void fordInWork(ActionEvent actionEvent) {
        if (!shouldGenerate && !shouldSetMatrix && !shouldBuild)
            fordInWork = true;

    }
    public void checkStartVert(MouseEvent event) throws IOException {
        if (fordInWork && !nextStep) {
            checkButton(event);
            for (Vertex vertex : buttons) {
                if (vertex.isChecked) {
                    double INFINITE = 10000;
                    startVert = vertex;

                    for (int i = 0; i < Vertex.i; i++) {
                        vec.add(i, INFINITE);
                    }
                    vec.set(vertex.getNumber(), 0.0);
                    cycleFord(vertex.getNumber(), Vertex.i, vec);
                    System.out.println("Ford for " + vertex.getNumber() + " was done!");
                    break;
                }
            }
        }
        //fordInWork=false;
    }


    public void nextStep(ActionEvent actionEvent) throws IOException {

        if(fordInWork) {
            nextStep = true;
            //Step.setDisable(false);
            cycleFord(startVert.getNumber(), Vertex.i, vec);
            if(last) {
                nextStep = false;
                fordInWork = false;
            }
        }
    }

    public void cycleFord (int s, int n, ArrayList<Double> vec) throws IOException
    {
        if(!last && fordInWork)
        {
            last = true;
            if (counter < edges.size())
                algFord(s, n,counter++, vec);
            else
                counter = 0;
        }
    }
    public void algFord(int s,int n, int i, ArrayList<Double> vec) throws IOException {
        double INFINITE = 10000;
                if (vec.get(mainController.edges.get(i).v1.getNumber()) < INFINITE) {

                    if (vec.get(mainController.edges.get(i).v1.getNumber()) + mainController.edges.get(i).getWeight() < vec.get(mainController.edges.get(i).v2.getNumber())) {
                        mainController.edges.get(i).line.setStyle("-fx-stroke:green");
                        vec.set((mainController.edges.get(i).v2.getNumber()), vec.get(mainController.edges.get(i).v1.getNumber()) + mainController.edges.get(i).getWeight());
                        last = false;
                    }
                }
                //BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                //String str = reader.readLine();
                //System.out.println(i);
                //  mainController.nextStep = false;

        for (int y = 0; y < n; y++)
        {
            System.out.println(s + "->" + y + ":" + (vec.get(y)!= INFINITE ?(vec.get(y)): " none"));
        }
    }

}
