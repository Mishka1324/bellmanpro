/**
 * Created by nikita on 28.06.16.
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Random;


public class generace {
    public ArrayList<ArrayList<Integer>> generateArr() throws IOException {

        System.out.println("Enter size of array(number of vershin): ");

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
         int size = Integer.parseInt(reader.readLine());



        ArrayList<ArrayList<Integer>> arr = new ArrayList<> ();
        for (int i = 0; i < size; i++) {
            ArrayList<Integer> temp = new ArrayList<> ();
            for (int j = 0; j < size; j++) {
                temp.add (j, 0);
            }
            arr.add (i,temp);
        }

        ravnomRaspred(arr, size);
        System.out.println(arr);
        return arr;
    }

    public void ravnomRaspred(ArrayList<ArrayList<Integer>> arr, int size){
        for (int i = 0; i < size; i++){
            for (int j = 0; j < size;j++) {
                if(i!=j) {
                    Random r = new Random();
                    arr.get(i).set(j, r.nextInt(10) + 1);

                }
                else
                    arr.get(i).set(j, 0);
            }
        }
    }
}

