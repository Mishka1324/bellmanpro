package controllers;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.RowConstraints;
import javafx.stage.Stage;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by User on 29.06.2016.
 */

public class matrixDialogController {

    public static int[][] matrix = new int[5][5];

    @FXML
    GridPane matrixGrid;

    private boolean isDigit(String text)
    {
        try{
            int number = Integer.parseInt(text);
            return true;
        }
        catch (NumberFormatException e) {
            return false;
        }
    }

    public void setMatrix(ActionEvent actionEvent) throws IOException {
       // String s = matrixGrid.getChildren().get(0).getId().;
        mainController.shouldSetMatrix = false;
        ((Stage) ((Node) actionEvent.getSource()).getScene().getWindow()).close();
    }

    public void closeMatrix(ActionEvent actionEvent) {
        mainController.shouldSetMatrix = false;
        ((Stage)((Node)actionEvent.getSource()).getScene().getWindow()).close();
    }

//    public static void inputMatrix(ActionEvent actionEvent) {
//        mainController.settingMatrixElements(actionEvent);
//    }

}
