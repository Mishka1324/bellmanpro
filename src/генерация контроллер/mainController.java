package controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;

import static java.lang.Math.sqrt;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import javafx.event.ActionEvent;

import javafx.stage.Modality;
import javafx.stage.Stage;
import objects.Vertex;
import objects.Edge;



public class mainController {

    public static ArrayList<Vertex> buttons = new ArrayList<Vertex>();

    public static ArrayList<Edge> edges = new ArrayList<Edge>();

    public static ArrayList<Double> vec = new ArrayList<Double>(Vertex.i);

    public static boolean shouldBuild = false;
    public static boolean shouldGenerate = false;
    public static boolean shouldSetMatrix = false;
    public static boolean fordInWork = false;
    public static boolean nextStep = false;

    public static int shpEdgesCounter = 0;

    public static boolean last = false;
    public static int counter = 0;
    public static Vertex startVert;


    @FXML
    Pane activePane;

    @FXML
    public Button Step;




    public void generateGraph(ActionEvent actionEvent) throws Exception {
        if (!(shouldBuild || shouldSetMatrix || fordInWork))
        shouldGenerate = true;
        if (shouldGenerate) {
            generation(actionEvent);
            System.out.println("shouldGenerate = true!");
        }
        else
            System.out.println("shouldGenerate = false!");
    }

    public void generation(ActionEvent actionEvent) throws Exception{
            Stage stage = new Stage();
            Parent panel = FXMLLoader.load(getClass().getResource("../fxml/generationDialog.fxml"));
            stage.setTitle("Слуыайная генерация.");
            stage.setMinWidth(200);
            stage.setMinHeight(150);
            stage.setResizable(false);
            stage.setScene(new Scene(panel));
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(((Node)actionEvent.getSource()).getScene().getWindow());
            stage.show();
    }



}
