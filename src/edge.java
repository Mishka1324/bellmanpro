import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.shape.Line;

import java.io.File;

/**
 * Created by alexandr on 28.06.2016.
 */
public class edge {
    private  Vertex _v1;
    private Vertex _v2;
    private int weight;
    public Line _line;
    public boolean isChecked;
    public edge(Vertex v1, Vertex v2, int newWeigth){
        this._v1 = v1;
        this._v2 = v2;
        this.isChecked=false;
        this.weight=newWeigth;
        this._line =createLine(v1,v2);
    }
    public Line createLine(Vertex v1,Vertex v2){
        Line line = new Line();
        line.setEndX(v1.circle.getX()+15);
        line.setStartX(v2.circle.getX()+15);
        line.setEndY(v1.circle.getY()+10);
        line.setStartY(v2.circle.getY()+10);
        line.setStyle("-fx-stroke-width:2");

        Media sound = new Media(new File("src/content/Edge.wav").toURI().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(sound);
        mediaPlayer.play();
        return line;
    }
    public Vertex getV1(){
        return this._v1;
    }
    public int getWeidth() { return this.weight; }
    public Vertex getV2(){
        return this._v2;
    }


}
