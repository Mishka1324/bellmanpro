package objects;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.shape.Line;

import java.io.File;

import static java.lang.Math.sqrt;

/**
 * Created by User on 29.06.2016.
 */
public class Edge {
    public Vertex v1;
    public Vertex v2;
    private double weight;
    public Line line= new Line();
    public boolean isChecked;

    public Edge(Vertex v1, Vertex v2, double weight) {
        this.v1 = v1;
        this.v2 = v2;
        this.weight = weight;
        this.isChecked = false;
        createLine (v1,v2);
    }

    public void createLine(Vertex v1, Vertex v2) {
        //Line line = new Line();
        //Polygon.
        this.line.setStartX(v1.circle.getX() + 25);
        this.line.setStartY(v1.circle.getY() + 25);
        this.line.setEndX(v2.circle.getX() + 25);
        this.line.setEndY(v2.circle.getY() + 25);
        this.line.setStyle("-fx-stroke-width:2");

        //Media sound = new Media(new File("").toURI().toString());
        //MediaPlayer mediaPlayer = new MediaPlayer(sound);
        //mediaPlayer.play();

    }

    public void syncronize (Vertex v1,Vertex v2) {
        this.v1 = v1;
        this.v2 = v2;
        changeLine(v1, v2);

    }

    public void changeLine (Vertex v1, Vertex v2) {
        //this.line.setLayoutX(300);
        this.line.setStartX(v1.circle.getX() + 25);
        this.line.setStartY(v1.circle.getY() + 25);
        this.line.setEndX(v2.circle.getX() + 25);
        this.line.setEndY(v2.circle.getY() + 25);
        this.weight =  sqrt(((v1.circle.getX() - v2.circle.getX())*(v1.circle.getX() - v2.circle.getX()))
                + ((v1.circle.getY() - v2.circle.getY())*(v1.circle.getY() - v2.circle.getY())));
    }

    public Vertex getV1() {
        return this.v1;
    }

    public Vertex getV2() {
        return this.v2;
    }

    public double getWeight() {
        return this.weight;
    }
}
