package objects;

import javafx.scene.image.ImageView;

import java.util.ArrayList;
/**
 * Created by User on 29.06.2016.
 */
public class Vertex {
    public static int i = 0;
    public ImageView circle;
    public boolean isChecked;
    private int number;

    public Vertex(ImageView circle)
    {
        this.number = i;
        this.circle=circle;
        isChecked=false;
        i++;

    }


    public int getNumber () {
        return this.number;
    }

}