package controllers;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;
import java.lang.*;
/**
 * Created by User on 29.06.2016.
 */
public class fordController {
    public static void algFord(int s,int n) throws IOException{
        int numb_of_edges;

        double INFINITE = 10000;

        ArrayList<Double> vec = new ArrayList<Double>(n);
        for (int i = 0; i < n; i++) {
            vec.add(i, INFINITE);
        }
        for (int i = 0; i < n; i++)
            mainController.edges.get(i).line.setStyle("-fx-stroke:blue");
        int relaxation_counter = 0;
        int compare_counter = 0;
        vec.set(s, 0.0);
        while (true) {

            boolean last = true;
            for (int i = 0; i < mainController.edges.size(); i++) {
                    if (vec.get(mainController.edges.get(i).v1.getNumber()) < INFINITE) {

                        if (vec.get(mainController.edges.get(i).v1.getNumber()) + mainController.edges.get(i).getWeight() < vec.get(mainController.edges.get(i).v2.getNumber())) {
                            mainController.edges.get(i).line.setStyle("-fx-stroke:green");
                            vec.set((mainController.edges.get(i).v2.getNumber()), vec.get(mainController.edges.get(i).v1.getNumber()) + mainController.edges.get(i).getWeight());
                            last = false;
                            relaxation_counter++;
                        }
                        compare_counter++;
                    }
              // }
                BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                String str = reader.readLine();
                System.out.println(i);
              //  mainController.nextStep = false;

            }
            if (last)
                break;
        }
        for (int i = 0; i < n; i++)
        {
            System.out.println(s + "->" + i + ":" + (vec.get(i)!= INFINITE ?(vec.get(i)): " none"));
        }
    }

}
