import javafx.scene.image.ImageView;
import javafx.scene.shape.Line;

import java.util.ArrayList;

/**
 * Created by Home-adm on 28.06.2016.
 */
public class frmArrToList {
    public ArrayList<edge> arrL=new ArrayList<edge>(0);
    private ArrayList frmArrToList(int[][]arr,int size){
        ImageView im = new ImageView("img/pluto.png");
        Vertex[] vert=new Vertex[size];
        int x=myPane.Width(),y=myPane.Height();
        for(int i=0;i<size;i++) {
            x+=(i*10)-10;
            y=x;
            vert[i].circle.setX(x);
            vert[i].circle.setY(y);
        }
        for(int i=0;i<size;i++) {
            for (int j = 0; j < size; j++) {
                edge edgeX = new edge(vert[i],vert[j],arr[i][j]);
                arrL.add(edgeX);
            }
        }
        return arrL;
    }
}
