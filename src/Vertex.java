import javafx.scene.image.ImageView;

import java.util.ArrayList;

/**
 * Created by alexandr on 28.06.2016.
 */
public class Vertex {
    static int i = 0;
    public ImageView circle;
    public boolean isChecked;

    Vertex (ImageView circle)
    {
        this.circle=circle;
        isChecked=false;
        i++;
    }

}