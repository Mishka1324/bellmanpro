package controllers;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.stage.Stage;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by User on 29.06.2016.
 */

public class matrixDialogController {

    @FXML
    GridPane gridMatrix;
    public void setMatrix(ActionEvent actionEvent) {
//        for(int i=0;i<10;i++)
//            gridMatrix.getRowConstraints();
        mainController.shouldSetMatrix = false;
        ((Stage) ((Node) actionEvent.getSource()).getScene().getWindow()).close();
    }

    public void closeMatrix(ActionEvent actionEvent) {
        mainController.shouldSetMatrix = false;
        ((Stage)((Node)actionEvent.getSource()).getScene().getWindow()).close();
    }
}
